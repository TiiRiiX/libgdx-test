package ru.fnight.libgdxtest.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import ru.fnight.libgdxtest.MyGdxGame;

import java.util.ArrayList;
import java.util.List;

public class MainMenuScreen implements Screen {

    final MyGdxGame game;

    private OrthographicCamera camera;
    private Stage stage;
    private List<TextButton> buttons = new ArrayList<TextButton>();

    public MainMenuScreen(final MyGdxGame game) {
        this.game = game;

        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        TextButton goToGameButton = makeButton("Go to level", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new GameScreen(game));
                dispose();
            }
        });
        goToGameButton.setPosition(Gdx.graphics.getWidth() / 2 - goToGameButton.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        stage.addActor(goToGameButton);
        buttons.add(goToGameButton);

        TextButton goToLevelButton = makeButton("Go to map", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new MapScreen(game));
                dispose();
            }
        });
        goToLevelButton.setPosition(Gdx.graphics.getWidth()/2 - goToLevelButton.getWidth()/2,
                Gdx.graphics.getHeight()/2 - goToLevelButton.getHeight());
        stage.addActor(goToLevelButton);
        buttons.add(goToLevelButton);

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private TextButton makeButton(String text, ChangeListener listener) {
        Skin skin = new Skin();
        skin.add("button", new Texture("img/blue_button01.png"));
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = game.font;
        textButtonStyle.up = skin.getDrawable("button");
        textButtonStyle.down = skin.getDrawable("button");
        textButtonStyle.checked = skin.getDrawable("button");
        TextButton button = new TextButton(text, textButtonStyle);
        button.addListener(listener);
        return button;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
