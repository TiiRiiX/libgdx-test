package ru.fnight.libgdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;

public class CoinObject {

    private Sprite sprite;
    private Body body;
    private int size;
    private Timer.Task task;

    public CoinObject(Sprite sprite, Body body, int size) {
        this.sprite = sprite;
        this.body = body;
        this.size = size;
    }

    public static CoinObject create(World world, int size, String imgPath) {
        Sprite sprite = new Sprite(new Texture(imgPath));
        Body body = createBodyCoin(world, size);
        return new CoinObject(sprite, body, size);
    }

    private static Body createBodyCoin(World world, int size) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

        Body body = world.createBody(bodyDef);

        CircleShape shape = new CircleShape();
        shape.setRadius(size);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 20;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;

        body.createFixture(fixtureDef);

        return body;
    }

    public void render(SpriteBatch batch) {
        sprite.setPosition(body.getPosition().x, body.getPosition().y);
        batch.draw(sprite, sprite.getX() - size * 2, sprite.getY() - size * 2,
                size * 4, size * 4);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Body getBody() {
        return body;
    }

    public Timer.Task getTask() {
        return task;
    }

    public void setTask(Timer.Task task) {
        this.task = task;
    }
}
