package ru.fnight.libgdxtest.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import ru.fnight.libgdxtest.CoinObject;
import ru.fnight.libgdxtest.MyGdxGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameScreen implements Screen {
    final MyGdxGame game;

    private static int COIN_SIZE = 25;
    private Texture ground;
    private List<CoinObject> coins = new ArrayList<CoinObject>();
    private World world = new World(new Vector2(0, -79), true);
    private CoinObject player;

    private OrthographicCamera camera;
    private Box2DDebugRenderer debugRenderer;

    private Timer.Task task;
    private Random random = new Random();

    private boolean isPause = false;

    public GameScreen(final MyGdxGame game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        debugRenderer = new Box2DDebugRenderer();

        ground = new Texture("img/grass.png");

        task = new Timer().scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                final CoinObject coinObject = CoinObject.create(world, COIN_SIZE, "img/coinSilver.png");
                Timer.Task removeTask = new Timer().scheduleTask(new Timer.Task() {
                    @Override
                    public void run() {
                        world.destroyBody(coinObject.getBody());
                        coins.remove(coinObject);
                    }
                }, 5, 0, 0);
                coinObject.setTask(removeTask);
                coinObject.getBody().applyForceToCenter(random.nextInt(10000000) - 5000000, 0, true);
                coins.add(coinObject);
            }
        }, 0, 2);

        createBodyGround();
        createWalls();

        player = CoinObject.create(world, COIN_SIZE, "img/coinGold.png");
        player.getBody().setTransform(100, 400, player.getBody().getAngle());
    }

    private void createWalls() {
        createGround(0, 0, 100, 300);
        createGround(Gdx.graphics.getWidth(), 0, 100, 300);
    }

    private Body createBodyGround() {
        return createGround(0, 0, Gdx.graphics.getWidth(), 100);
    }

    private Body createGround(int x, int y, int width, int height) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(x, y));
        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);
        body.createFixture(shape, 0);

        return body;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.isKeyPressed(Input.Keys.GRAVE)) {
            debugRenderer.render(world, camera.combined);
        }

        update(Gdx.graphics.getDeltaTime());
    }

    private void update(float delta) {
        if (isPause) {
            delta = 0;
        }
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        world.step(delta, 6, 2);

        inputUpdate();

        game.batch.begin();
        game.batch.draw(ground, 0, 0, Gdx.graphics.getWidth(), 100);
        game.batch.draw(ground, 0, 0, 100, 300);
        game.batch.draw(ground, Gdx.graphics.getWidth() - 100, 0, 100, 300);
        player.render(game.batch);
        for (CoinObject coin : coins) {
            coin.render(game.batch);
        }
        game.batch.end();
    }

    private void inputUpdate() {
        int horizontalForce = 0;

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            horizontalForce -= 1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            horizontalForce += 1;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            player.getBody().applyForceToCenter(0, 300000000, false);
        }

        if (!isPause && Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            pause();
        } else if (isPause && Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            resume();
        }

        player.getBody().setLinearVelocity(horizontalForce * 50, player.getBody().getLinearVelocity().y);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {
        isPause = true;
        task.cancel();
    }

    @Override
    public void resume() {
        isPause = false;
        task.run();
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
