package ru.fnight.libgdxtest.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import ru.fnight.libgdxtest.MyGdxGame;
import ru.fnight.libgdxtest.PlayerObject;

public class MapScreen implements Screen {

    final MyGdxGame game;

    private OrthographicCamera camera;
    private Viewport viewport;
    private TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;

    private Box2DDebugRenderer debugRenderer;

    private World world = new World(new Vector2(0, -10), true);

    private PlayerObject player;

    public MapScreen(MyGdxGame game) {
        this.game = game;

        debugRenderer = new Box2DDebugRenderer();
        camera = new OrthographicCamera();
        viewport = new ExtendViewport(20, 20, camera);

        map = new TmxMapLoader().load("tilemaps/level.tmx");

        float scale = 1 / 70f;

        mapRenderer = new OrthogonalTiledMapRenderer(map, scale);

        MapLayer marks = map.getLayers().get("Marks");
        MapObjects layerObjects = marks.getObjects();
        MapObject start = layerObjects.get("start");
        Float xStart = start.getProperties().get("x", Float.class);
        Float yStart = start.getProperties().get("y", Float.class);

        player = PlayerObject.create(world, xStart * scale, yStart * scale, 1.26f, 1.8f);

        map.getLayers().get("Bodies").getObjects().forEach(mapObject -> {
            if (mapObject instanceof RectangleMapObject) {
                RectangleMapObject rectangleMapObject = (RectangleMapObject) mapObject;
                float x = rectangleMapObject.getRectangle().x * scale;
                float y = rectangleMapObject.getRectangle().y * scale;
                float height = rectangleMapObject.getRectangle().height * scale;
                float width = rectangleMapObject.getRectangle().width * scale;
                BodyDef bodyDef = new BodyDef();
                bodyDef.position.set(new Vector2(x + width / 2, y + height / 2));
                Body body = world.createBody(bodyDef);

                PolygonShape shape = new PolygonShape();
                shape.setAsBox(width / 2, height / 2);
                body.createFixture(shape, 0);
            }
        });
    }

    private void setCameraPosition(float x, float y) {
        camera.position.x = x;
        camera.position.y = y;
        camera.update();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        inputUpdate();

        if (Gdx.input.isKeyPressed(Input.Keys.GRAVE)) {
            debugRenderer.render(world, camera.combined);
        }

        setCameraPosition(player.getBody().getPosition().x, player.getBody().getPosition().y);

        mapRenderer.setView(camera);
        mapRenderer.render();
        game.batch.setProjectionMatrix(camera.combined);

        world.step(delta, 6, 2);

        game.batch.begin();
        player.render(game.batch);
        game.batch.end();
    }

    private void inputUpdate() {
        int horizontalForce = 0;

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            horizontalForce -= 1;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            horizontalForce += 1;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            player.getBody().applyForceToCenter(0, 200f, false);
        }

        player.getBody().setLinearVelocity(horizontalForce * 5, player.getBody().getLinearVelocity().y);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
