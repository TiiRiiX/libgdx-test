package ru.fnight.libgdxtest;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class PlayerObject {

    private Sprite sprite;
    private Body body;
    private float width;
    private float height;

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public PlayerObject(Sprite sprite, Body body, float width, float height) {
        this.sprite = sprite;
        this.body = body;
        this.width = width;
        this.height = height;
    }

    public static PlayerObject create(World world, float x, float y, float width, float height) {
        Sprite sprite = new Sprite(new Texture("img/alienPink_walk1.png"));
        Body body = createBody(world, x, y, width, height);
        return new PlayerObject(sprite, body, width, height);
    }

    public static Body createBody(World world, float x, float y, float width, float height) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);

        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width/2, height/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = 0.3f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;

        body.createFixture(fixtureDef);

        return body;
    }

    public void render(SpriteBatch batch) {
        sprite.setPosition(body.getPosition().x - width/2, body.getPosition().y - height/2);
        batch.draw(sprite, sprite.getX(), sprite.getY(), width, height);
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Body getBody() {
        return body;
    }
}
