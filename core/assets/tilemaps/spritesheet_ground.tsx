<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="spritesheet_ground" tilewidth="128" tileheight="128" tilecount="128" columns="8">
 <image source="spritesheet_ground.png" width="1024" height="2048"/>
 <tile id="0">
  <properties>
   <property name="blended" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="blended" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
